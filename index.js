const Server =  require('./Server').Server;
const minimist =  require('minimist');

const argv = minimist(process.argv, {
  'default': {
    'server-port': 8080
  }
});

const server = new Server(argv['server-port']);
server.listen();
