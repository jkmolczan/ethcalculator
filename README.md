# Etherum Blockchain Calculator

This is a little demo app calculates an average time of adding a block to the ethereum blockchain.

You could run the app either using machine dependnecies, or using docker.

## Running using local machine

### Installing

Dependencies:

* [Node.js](https://nodejs.org/en/) v9.8.0 or above

Then check out the project and run:

```sh
npm install
```

### Running

```sh
npm start
```

### Using the app

You should now have the app server at [localhost:8080](http://localhost:8888).

You can also configure the port:

```sh
npm start -- --server-port=8000
```

## Running using docker

```sh
docker-compose up
```

Here also you should have the app server at [localhost:8080](http://localhost:8080).

You can configure the port by changing it in `docker-compose.yml` before starting:

```yml
port:
  # <host>:<container>
  - 8000:8080
```

## Troubleshooting

* If you get any node-sass errors, try remove `node_modules` folder and run `npm install` again.

## Tests

### Running

```sh
npm test
```

### TODO list:
* Tests improve
