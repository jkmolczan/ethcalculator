const EthCalculator = require('../EthCalculator').EthCalculator;

test('Test EthCalculator class init.', () => {
    const ethCalculator = new EthCalculator();
    expect(ethCalculator).toBeDefined();
    expect(ethCalculator.web3).toBeTruthy();
    expect(ethCalculator.blocksCount).toBeTruthy();
});

