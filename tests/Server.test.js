const Server = require('../Server').Server;

test('Test server class init.', () => {
    const server = new Server(8080);
    expect(server).toBeDefined();
});
