const Web3 = require('web3');


class EthCalculator {
    constructor () {
        // fake account on infura.io
        this.web3 = new Web3(new Web3.providers.HttpProvider('https://ropsten.infura.io/OTiSF0r3jdcvKjZ6BnFW'));
        // calculate time for last blocksCount blocks
        this.blocksCount = 20;
    }
    calculate() {
        return this.web3.eth.getBlock("latest").then(result => {
            return this.fetchBlocksTimes(result['number']).then(blockTimes => {
                let blockTimeRange = 0;
                for (let i = 1, l = blockTimes.length; i < l; i++) {
                    blockTimeRange += blockTimes[i -1] - blockTimes[i];
                }
                return (blockTimeRange/this.blocksCount);
            }).catch(err => {
                console.log('EthCalculator/calculate: error while fetching blocks data, error message: ', err);
                throw 'Error during fetching blocks data.';
            });
        }).catch(err => {
            console.log('EthCalculator/calculate: error while fetching latest block data, error message: ', err);
            throw 'Error during fetching latest block data.';
        });
    }
    // TODO: add some logging, catch errors
    fetchBlocksTimes(blockNumber) {
        let promises = [];
        console.log('EthCalculator/fetchBlocksTimes: fetching blocks data for blocks %s to %s', blockNumber -this.blocksCount, blockNumber);
        for (let i = 0; i < this.blocksCount; i++) {
            promises.push(this.web3.eth.getBlock(blockNumber - i));
        }
        return Promise.all(promises).then(results => {
            console.log('EthCalculator/fetchBlocksTimes: blocks data fetched');
            let blocksTimes = [];
            for (let i = 0, l = results.length; i < l; i++) {
                const obj = results[i];
                blocksTimes.push(obj['timestamp']);
            }
            return blocksTimes;
        }).catch(err => {
            console.log('EthCalculator/fetchBlocksTimes: error while fetching blocks, errors:', err);
            throw 'Error while fetching blocks'
        });
    }
}

exports.EthCalculator = EthCalculator;