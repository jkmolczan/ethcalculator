const express = require('express');
const EthCalculator = require('./EthCalculator').EthCalculator;


class Server {
    constructor(port) {
        this._app = express();
        this._port = port;

        this._app.get('/', (req, resp) => {
            resp.header("Access-Control-Allow-Origin", "*");
            const ethCalc = new EthCalculator();
            ethCalc.calculate()
                .then(avgTime => resp.send(avgTime.toString()))
                .catch(err => {
                    console.log('Server/get: error during calculate avg time, error message: ', err);
                    resp.status(500).send('Something went wrong.');
                })
        });
    }

    listen() {
        this._app.listen(this._port, _ => {
            console.log("Server listening at localhost:" + this._port);
        });
    }
}

exports.Server = Server;
